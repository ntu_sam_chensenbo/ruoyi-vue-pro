import BasicInfoForm from './BasicInfoForm.vue'
import CloumInfoForm from './CloumInfoForm.vue'
import EditTable from './EditTable.vue'
import GenInfoForm from './GenInfoForm.vue'
import ImportTable from './ImportTable.vue'
import Preview from './Preview.vue'
export { BasicInfoForm, CloumInfoForm, EditTable, GenInfoForm, ImportTable, Preview }
